package com.ucbcba.proyecto.proyecto.Entities;

import com.sun.istack.internal.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "Pais")
public class Pais {



    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idPais;
    @NotNull
    @Size(min = 1, max = 50, message = "Debe tener ser menos a 50 caracteres")
    private String name;
   /*@OneToMany (mappedBy = "paise")
   private Set<Ciudad> ciudades;
   public Set<Ciudad> getidCiudad() {return ciudades;   }
   public void setCiudad(Set<Ciudad> ciudad) {this.ciudades = ciudad;}

*/

    public Integer getidPais() {
        return idPais;
    }

    public void setidPais(Integer idPais) {
        this.idPais = idPais;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
